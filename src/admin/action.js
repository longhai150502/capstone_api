
let PRODUCT_URL = "https://635f4b62ca0fe3c21a992480.mockapi.io"
let idEdit = null;

let fetchAllProduct = () =>{
    axios({
        url: `${PRODUCT_URL}/products`,
        method: "GET",
    })
        .then((res) => {
            renderProductList(res.data);
        })
        .catch((err) => {
            console.log('err', err)
        })
}
fetchAllProduct()

let addProduct = () => {
    let data = layThongTinTuForm()

    let newProduct = {
        name: data.name,
        price: data.price,
        img: data.img,
        desc: data.desc,
    }
    axios({
        url: `${PRODUCT_URL}/products`,
        method: "POST",
        data: newProduct,
    })
        .then(function(res){
            fetchAllProduct();
            console.log('res', res);
        })
        .catch(function(err){
            console.log('err', err)
        })
}

let removeProduct = (idProduct) =>{
    axios({
        url: `${PRODUCT_URL}/products/${idProduct}`,
        method: 'DELETE',
    })
        .then(function(res){
            fetchAllProduct()
            console.log('res', res);
        })
        .catch(function(err){
            console.log('err', err)
        })
}

let editProduct = (idProduct) => {
    axios({
        url: `${PRODUCT_URL}/products/${idProduct}`,
        method: 'GET',
    })
        .then(function(res){
            document.getElementById("TenSP").value = res.data.name;
            document.getElementById("GiaSP").value = res.data.price;
            document.getElementById("HinhSP").value = res.data.img;
            document.getElementById("MoTa").value = res.data.desc;
            idEdit = res.data.id;
        })
        .catch(function(err){
            console.log('err', err)
        })
}

let updateProduct = () => {
    let data = layThongTinTuForm()
    axios ({
        url: `${PRODUCT_URL}/products/${idEdit}`,
        method: "PUT",
        data: data,
    })
        .then((res) => {
            fetchAllProduct()
        })
        .catch((err) => {
        });
}

let filterProduct = () => {
    
    axios({
        url: `${PRODUCT_URL}/products`,
        method: "GET",
    })
        .then((res) => {
            let productByName = [];
            let searchValue = document.getElementById("inputTK");
            for( let i = 0; i < res.data.length; i++){
                if(res.data[i].name == searchValue.value){
                    productByName.push(res.data[i]);
                    console.log(productByName);
                }
            }
            renderProductList(productByName);
        })
        .catch((err) => {
            console.log('err', err)
        })
}
