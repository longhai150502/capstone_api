let renderProductList = (list) =>{
    let contentHTML = "";
    list.forEach(element => {
        contentHTML +=`
            <tr>
                <td>${element.id}</td>
                <td>${element.name}</td>
                <td>${element.price}</td>
                <td><img class="img-admin" src="${element.img}" alt="" /></td>
                <td>${element.desc}</td>
                <td>
                    <button onclick="removeProduct(${element.id})" class="btn btn-danger">Xóa</button>
                    <button onclick = "editProduct(${element.id})" class="btn btn-warning" data-toggle="modal" data-target="#myModal">Edit</button>
                </td>
            </tr>
        `
    });
    document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
}


let layThongTinTuForm = () => {
    var TenSP = document.getElementById("TenSP").value;
    var GiaSP = document.getElementById("GiaSP").value ;
    var HinhSP = document.getElementById("HinhSP").value ;
    var MoTa = document.getElementById("MoTa").value ;
     return {
         name: TenSP,
         price: GiaSP,
         img: HinhSP,
         desc: MoTa,
     }
 }

let resetForm = () => {
    document.getElementById("myForm").reset();
 }
