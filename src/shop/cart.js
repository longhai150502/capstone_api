let headerCart = document.getElementById("headerCart");
let ShoppingCart = document.getElementById("shopping-cart");

let cartProduct = JSON.parse(localStorage.getItem("data")) || [];

let calculation = () => {
  let cartIcon = document.getElementById("cartAmount");
  cartIcon.innerHTML = cartProduct.map((x) => x.item).reduce((x, y) => x + y, 0);
};

calculation();

let renderCartItems = () => {
  if (cartProduct.length !== 0) {
    return (ShoppingCart.innerHTML = cartProduct
      .map((cartItems) => {
        let { id, item } = cartItems;
        let search = shopItemsData.find((item) => item.id === id) || [];
        return `
      <div class="cart-item">
        <img width="100" src=${search.img} alt="" />
        <div class="details">

          <div class="title-price-x">
              <h4 class="title-price">
                <p>${search.name}</p>
                
              </h4>
              <i onclick="removeItem(${id})" class="bi bi-trash"></i>
          </div>

          <div class="buttons">
              <i onclick="decrement(${id})" class="bi bi-dash-lg"></i>
              <div id=${id} class="quantity">${item}</div>
              <i onclick="increment(${id})" class="bi bi-plus-lg"></i>
          </div>

          <h3>$ ${item * search.price}</h3>
        </div>
      </div>
      `;
      })
      .join(""));
  } else {
    ShoppingCart.innerHTML = ``;
    label.innerHTML = `
    <h2>Cart is Empty</h2>
    <a href="index.html">
      <button class="HomeBtn">Back to home</button>
    </a>
    `;
  }
};
renderCartItems();

let increment = (id) => {
  let selectedItem = id;
  let search = cartProduct.find((item) => item.id === selectedItem.id);

  if (search === undefined) {
    cartProduct.push({
      id: selectedItem.id,
      item: 1,
    });
  } else {
    search.item += 1;
  }

  renderCartItems();
  update(selectedItem.id);
  localStorage.setItem("data", JSON.stringify(cartProduct));
};
let decrement = (id) => {
  let selectedItem = id;
  let search = cartProduct.find((product) => product.id === selectedItem.id);

  if (search === undefined) return;
  else if (search.item === 0) return;
  else {
    search.item -= 1;
  }
  update(selectedItem.id);
  cartProduct = cartProduct.filter((x) => x.item !== 0);
  renderCartItems();
  localStorage.setItem("data", JSON.stringify(cartProduct));
};

let update = (id) => {
  let search = cartProduct.find((item) => item.id === id);
  document.getElementById(id).innerHTML = search.item;
  calculation();
  TotalAmount();
};

let removeItem = (id) => {
  let selectedItem = id;
  cartProduct = cartProduct.filter((x) => x.id !== selectedItem.id);
  calculation();
  renderCartItems();
  TotalAmount();
  localStorage.setItem("data", JSON.stringify(cartProduct));
};

let clearCart = () => {
  cartProduct = [];
  calculation();
  renderCartItems();
  localStorage.setItem("data", JSON.stringify(cartProduct));
};

let TotalAmount = () => {
  if (cartProduct.length !== 0) {
    let amount = cartProduct
      .map((element) => {
        let { item, id } = element;
        let search = shopItemsData.find((item) => item.id === id) || [];

        return item * search.price;
      })
      .reduce((element, item) => element + item, 0);
    // console.log(amount);
    label.innerHTML = `
    <h2>Total Bill : $ ${amount}</h2>
    <button class="checkout">Purchase</button>
    <button onclick="clearCart()" class="removeAll">Clear Cart</button>
    `;
  } else return;
};
TotalAmount();
