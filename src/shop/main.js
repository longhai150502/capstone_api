let shop = document.getElementById("shop");

let cartProduct = JSON.parse(localStorage.getItem("data")) || [];
console.log(cartProduct)

let renderShop = () => {
  return (shop.innerHTML = shopItemsData
    .map((element) => {
      let { id, name, price, desc, img,} = element;
      let search = cartProduct.find((element) => element.id === id) || [];
      return `
    <div id=product-id-${id} class="item">
        <img class="imgProduct" width="220" src=${img} alt="">
        <div class="details">
          <h3>${name}</h3>
          <p>${desc}</p>
          <div class="price-quantity">
            <h2>$ ${price} </h2>
            <div class="buttons">
              <i onclick="decrement(${id})" class="bi bi-dash-lg"></i>
              <div id=${id} class="quantity">
              ${search.item === undefined ? 0 : search.item}
              </div>
              <i onclick="increment(${id})" class="bi bi-plus-lg"></i>
            </div>
          </div>
        </div>
      </div>
    `;
    })
    .join(""));
};
renderShop();

let increment = (id) => {
  let selectedItem = id;
  let search = cartProduct.find((x) => x.id === selectedItem.id);

  if (search === undefined) {
    cartProduct.push({
      id: selectedItem.id,
      item: 1,
    });
  } else {
    search.item += 1;
  }

  console.log(cartProduct);
  update(selectedItem.id);
  localStorage.setItem("data", JSON.stringify(cartProduct));
};
let decrement = (id) => {
  let selectedItem = id;
  let search = cartProduct.find((x) => x.id === selectedItem.id);

  if (search === undefined) return;
  else if (search.item === 0) return;
  else {
    search.item -= 1;
  }
  update(selectedItem.id);
  cartProduct = cartProduct.filter((x) => x.item !== 0);
  // console.log(cartProduct);
  localStorage.setItem("data", JSON.stringify(cartProduct));
};
let update = (id) => {
  let search = cartProduct.find((x) => x.id === id);
  document.getElementById(id).innerHTML = search.item;
  calculation();
};

let calculation = () => {
  let cartIcon = document.getElementById("cartAmount");
  cartIcon.innerHTML = cartProduct.map((x) => x.item).reduce((x, y) => x + y, 0);
};

calculation();




let typeProduct = document.getElementById("selectType");


let renderProductByType = (arr) => {
  return (shop.innerHTML = arr
    .map((x) => {
      let { id, name, price, desc, img,} = x;
      let search = cartProduct.find((x) => x.id === id) || [];
      return `
    <div id=product-id-${id} class="item">
        <img class="imgProduct" width="220" src=${img} alt="">
        <div class="details">
          <h3>${name}</h3>
          <p>${desc}</p>
          <div class="price-quantity">
            <h2>$ ${price} </h2>
            <div class="buttons">
              <i onclick="decrement(${id})" class="bi bi-dash-lg"></i>
              <div id=${id} class="quantity">
              ${search.item === undefined ? 0 : search.item}
              </div>
              <i onclick="increment(${id})" class="bi bi-plus-lg"></i>
            </div>
          </div>
        </div>
      </div>
    `;
    })
    .join(""));
};


let filterProduct = () => {
  let newShop = [];
  for( let i = 0; i< shopItemsData.length; i++){
    if(typeProduct.value == shopItemsData[i].type){
      newShop.push(shopItemsData[i]);
      renderProductByType(newShop);
    }
    if(typeProduct.value=="") {
      renderShop();
    }
  }
}
